import numpy as np
import copy
import random

import shelve

from sklearn.utils.graph_shortest_path import graph_shortest_path
from skimage.draw import polygon

class complex_room():
    """A class representing a complex room in matrix form"""
    def __init__(self, room_shape=(100,100)):
        self.set_data()
        self.set_room(room_shape[0], room_shape[1])

    def set_data(self):
        self.FREE=0
        self.OBS=1
        self.HOMO=False
        self.ALT=True
        self.FAIL=-3
        self.ROUT=2

    @classmethod
    def from_file(cls, FILE_PATH):
        "Create a binary matrix from a bitmap image"
        import os
        import imageio

        absolute_path = os.path.join(FILE_PATH)
        image = imageio.imread(absolute_path, as_gray=0)
        if len(image.shape)>2:
            raw_room = np.array(image[:,:,0])
        else:
            raw_room = np.matrix(image)

        cor = cls(raw_room.shape)
        cor.set_map_name(FILE_PATH[:-4])
        for y in range(raw_room.shape[0]):
            for x in range(raw_room.shape[1]):
                if raw_room[y,x] == 255:
                    cor.room[y,x] = cor.FREE
                else:
                    cor.room[y,x] = cor.OBS
        return cor

    @classmethod
    def from_file(cls, nd_array):
        #Takes a binary nd_array as input
        cor = cls(nd_array.shape)
        cor.room = nd_array
        return cor

    def set_map_name(self, map_name):
        self.map_name = map_name
        #print("Set map name to", map_name)

    def get_map_name(self):
        return self.map_name

    def set_room(self,y,x):
        """Initialize room with shape (y,x)"""
        new_room=[]

        for row in range(y):
            if row==0 or (y-row)==1:
                new_room.append([col for col in range(x)])
            else:
                new_room.append([row]+[self.FREE]*(x-2)+[row])
        self.room=np.matrix(new_room)

    def get_room(self):
        """Return Room as a numpy nd-array"""
        return self.room

    def room_shape(self):
        """Return a tuple (rows,cols) representing the shape of the room matrix"""
        return self.get_room().shape

    def rand_pos(self):
        """Returns random walkable position"""
        return int(random.random()*(self.room_shape()[0]-1)), int(random.random()*(self.room_shape()[1]-1))

    def add_obstacle(self,position,size):
        """Place rectangle-shaped obstacle"""
        new_room=copy.deepcopy(self.room)
        try:
            for y in range(size[1]):
                for x in range(size[0]):
                    new_room[(y+position[1]),(x+position[0])]=self.OBS
        except Exception as e:
            print("Problem:",e,(y,x))
        else:
            self.room=new_room

    def check_homotopy_simple(self, route, alternative):
        """Calculate homotpy relation for 2 routes"""
        #CHECK: Starting and Ending Points need to be equal
        if not self.get_start(route) == self.get_start(alternative):
            print("Not same Start")
            raise(HomotopyException())
        #if not self.get_end(route) == self.get_end(alternative):
        end_r=route[-1]
        end_a=alternative[-1]
        if end_r != end_a:
            print(route)
            print(alternative)
            print("Not same End")
            raise(HomotopyException())
        #CHECK: Routes must be valid
        if not self.check_route_simple(route):
            print("Route not valid")
            plt.figure()
            plt.imshow(self.draw_route_simple(route), cmap="gray")
            plt.show()
            return self.FAIL
            #raise(HomotopyException())
        if not self.check_route_simple(alternative):
            print("Alternative Route not valid")
            plt.imshow(self.draw_route_simple(alternative), cmap="gray")
            return self.FAIL
            #raise(HomotopyException())

        label=self.fill_space_simple(route, alternative)[0]
        return label


    def check_route_simple(self, route):
        """Check if route is valid"""
        last=(None,None)
        for point in route:
            x = point[1]
            y = point[0]
            if x > self.room.shape[1]:
                print("X > shape",x, self.room.shape[1])
                return False
            if y > self.room.shape[0]:
                print("Y > shape",y, self.room.shape[1])
                return False
            if last[1] and not self.is_neighbor((x,y),last):
                print((x,y),"is not a neighbor of", tuple(last))
                return False
            if self.room[y,x] == self.OBS:
                return False
            last = (x,y)
        return True

    def draw_route_simple(self, route):
        """Writes route onto the matrix"""
        #if not self.check_route_simple(route):
        #    return False
        room_clone=copy.deepcopy(self.room)
        for point in route:
            x = point[1]
            y = point[0]
            room_clone[y,x] = self.ROUT if self.room[y,x] == self.FREE else -self.ROUT
        return room_clone

    def is_neighbor(self, p1, p2):
        """Check if 2 positions are neighbors"""
        dif_row = abs(p1[0]-p2[0])
        dif_col = abs(p1[1]-p2[1])
        return (dif_row == 1 and dif_col == 0) or (dif_col == 1 and dif_row == 0) or (dif_col == 1 and dif_row == 1)

    def get_start(self,route_sparse):
        return route_sparse[0]

        y=None
        x=None
        for i in range(len(route_sparse)):
            if route_sparse[i]:
                y=i
                break
        for j in range(len(route_sparse[i])):
            if route_sparse[i][j]:
                x=j
                break
        if x and y:
            return (x,y)


    def get_end(self,route_sparse):
        return route_sparse[1]
        y=None
        x=None
        for i in reversed(range(len(route_sparse))):
            if route_sparse[i]:
                y=i
                break
        for j in reversed(range(len(route_sparse[i]))):
            if route_sparse[i][j]:
                x=route_sparse[i][j]
                break
        if x and y:
            return (x,y)

    def concat_simple(self, route, alternative):
        """Concatenate two routes to a polygon"""
        return route + list(reversed(alternative))[1:]

    def fill_space_simple(self, route, alternative):
        """Calculate Homotopy Relation, by applying the homotopy-mask-method [Werner et al. 2014]"""
        import matplotlib.pyplot as plt
        from visualize import plt_route
        concat = route+list(reversed(alternative))
        polygon_map = np.zeros(self.get_room().shape)
        rr = [r for r,_ in concat]
        cc = [c for _,c in concat]
        rp, cp = polygon(rr, cc)

        polygon_map[rp,cp] = 1

        mask = np.multiply(self.get_room(), polygon_map)

        if self.OBS in mask:
            return self.ALT, None
        else:
            return self.HOMO, None

    def overlap_area(self, route, alternative):
        """Return area size inbetween a route pair"""
        polygon_map = np.zeros(self.get_room().shape)
        concat = route + list(reversed(alternative))[1:]
        #fill edges
        rr = [r for r,_ in concat]
        cc = [c for _,c in concat]
        polygon_map[rr,cc] =1
        #fill space between edges
        rp, cp = polygon(rr, cc)
        polygon_map[rp,cp] = 1
        area = np.count_nonzero(polygon_map==1)
        return area

    def overlap_edges(self, route, alternative):
        """Return amount of overlapping edges of a route pair"""
        route = [(r,c) for r,c in route]
        alternative =[(r,c) for r,c in alternative]
        return len({e for e in route if e in alternative})

    #### ROUTE GENERATOR
    def djkstra_simple(self, graph, start, goal):
        """Return shortest path from start to goal"""
        import networkx as nx
        return nx.dijkstra_path(graph, start, goal)

######### TODO

    def penalty(self, bitgraph, start, goal, limit=1, increment=10):
        """Generates a batch of routes using the penalty method [Werner et al. 2014]"""
        routes=list()

        if not bitgraph.exists_path(start, goal):
            return routes
        for _ in range(limit):
            shortest_path = self.djkstra_simple(bitgraph.graph, start, goal)
            routes.append(shortest_path)
            last = None
            for i, _ in enumerate(shortest_path):
                if i+1 < len(shortest_path):
                    pos = shortest_path[i]
                    neig = shortest_path[i+1]
                    bitgraph.graph[pos][neig]['weight'] += increment
                    #print(bitgraph.graph[pos][neig]['weight'])
        return routes


    def one_patching(self, bitgraph, start, goal, limit=1):
        """Generates a batch of routes using the one-patching method [Werner et. al 2014]"""
        routes=list()
        if not bitgraph.exists_path(start, goal):
            return routes()
        routes.append(self.djkstra_simple(bitgraph.graph, start, goal))
        from random import random
        for _ in range(limit):
            check=False
            while not check:
                crossover_point = self.rand_pos()
                while not (bitgraph.exists_path(start, crossover_point) and bitgraph.exists_path(crossover_point, goal)):
                    crossover_point = self.rand_pos()
                try:
                    route1 = self.djkstra_simple(bitgraph.graph,start,crossover_point)
                    route2 = self.djkstra_simple(bitgraph.graph,crossover_point,goal)
                    #Check is possibly redundant
                    check = self.check_route_simple(route1) and self.check_route_simple(route2) and len(route1)>1 and len(route2)>1
                except:
                    print("Error in DJKSTRA")
                else:
                    routes.append(route1[:-1] + route2)
        return routes

class HomotopyException(Exception):
    pass

class RoomBuilderException(Exception):
    pass


import networkx as nx
import numpy as np
import copy
import random
from math import *


class bit_graph():
    """A class representing a complex room in graph form"""

    def __init__(self, complex_room, file_name=None):
        self.complex_room = complex_room
        self.bitmap=copy.deepcopy(complex_room.get_room())
        if file_name:
            #store graph to disc
            with shelve.open("_data/shelves/graphs/"+file_name) as graph_file:
                if "graph" in graph_file:
                    self.graph = graph_file["graph"]
                else:
                    self.graph = self._build_graph()
                    graph_file["graph"] = self.graph
        else:
            self.graph = self._build_graph()

    def _build_graph(self, full_neighbors=True):
        graph = nx.Graph()
        # Do checks in order: up - left - upperLeft - lowerLeft
        neighbors = [(0, -1, 1), (-1, 0, 1), (-1, -1, sqrt(2)), (-1, 1, sqrt(2))]
        # Check pixels for their color (determine if walkable)
        for idx, value in np.ndenumerate(self.bitmap):
            if value == self.complex_room.FREE:
                try:
                    y, x = idx
                except ValueError:
                    y, x, channels = idx
                    idx = (y, x)
                # IF walkable, add node
                graph.add_node((y, x), count=0)
                # Fully connect to all surrounding neighbors
                for n, (xdif, ydif, weight) in enumerate(neighbors):
                    # Differentiate between 8 and 4 neighbors
                    if not full_neighbors and n >= 2:
                        break

                    query_node = (y + ydif, x + xdif)
                    if graph.has_node(query_node):
                        graph.add_edge(idx, query_node, weight=weight)
        return graph

    def simple_trajectory_between(self, start, dest):
        """Return shortest-path between start and dest"""
        return nx.dijkstra_path(self.graph, start, dest)

    def get_valid_position(self):
        valid_position = random.choice(list(self.graph.nodes))
        return valid_position

    def get_random_vertices(self):
        """Return 2 random positions that are connected"""
        start = self.get_valid_position()
        dest = self.get_valid_position()
        while not self.exists_path(start, dest):
            start = self.get_valid_position()
            dest = self.get_valid_position()
        return (start, dest)

    def get_random_trajectory(self):
        """Return random existing trajectory"""
        start, dest = self.get_random_vertices()
        return self.simple_trajectory_between(start, dest)

    def exists_path(self, pointA, pointB):
        """Checks if 2 points are connected"""
        if pointA in self.graph and pointB in self.graph:
            if nx.has_path(self.graph, pointA, pointB):
                return True
        else:
            return False

    def pos_to_id(self, pos):
        """Coordinate Form --> Constant Form"""
        return pos[0] * (self.bitmap.shape[1]) + pos[1]

    def id_to_pos(self, id):
        """Constant Form --> Coordinate Form"""
        side_len = self.bitmap.shape[1]
        row = int(id/side_len)
        col = int(id%side_len)
        return row, col


def check_set_homotopy(cr, routes):
    """Calculate Homotopy relation between all routes in a set"""
    alternatives=set()
    homotopic = set()
    for n in range(len(routes)):
        for m in range(len(routes)):
            if n != m:
                val = cr.check_homotopy_simple(routes[n],routes[m])
                if val == cr.HOMO:
                    homotopic.add(tuple(sorted((n,m))))
                if val == cr.ALT:
                    alternatives.add(tuple(sorted((n,m))))
    #print("Alternative Routes:",len(alternatives))
    #print("Homotopic Routes:", len(homotopic))
    #print("Routes Total:", len(routes))

    return homotopic, alternatives

def gen_samples(file_name, limit, algorithm="one_patching"):
    """Generates a batch of classified route pairs and stores them to file"""
    path_maps_raw = "dataset/maps/raw/"
    path_routes_raw = "dataset/routes/raw/"

    cor = complex_room.from_file(path_maps_raw + file_name)
    bg = bit_graph(cor,file_name=file_name[:-4])

    start, goal = cor.rand_pos(), cor.rand_pos()
    while not bg.exists_path(start, goal):
        start, goal = cor.rand_pos(), cor.rand_pos()

    if algorithm == "one_patching":
        alt_routes = cor.one_patching(bg, start, goal, limit)
    elif algorithm == "penalty":
        alt_routes = cor.penalty(bg, start, goal, limit)
    else:
        raise(ValueError(algorithm + " is not a defined route-generating-algorithm"))
    homs , alts = check_set_homotopy(cor, alt_routes)
    list_only = lambda tset : [[x,y] for x,y in tset]

    data = {"routes":alt_routes,"homotopic":list_only(homs), "alternative":list_only(alts)}

    return data
    #with open(path_routes_raw+file_name[:-4]+".json", "w") as data_file:
    #    json.dump(data, data_file)
