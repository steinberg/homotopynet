# HomotopyNet

Code for my Bachelor's Thesis:
Supervised learning of a homotopy relation for 2d-trajectories.

This project implements a dataset of homotopy-labeled route pairs in constrained free spaces,
as well as several models of artificial neural networks designed to compare route-pairs and models.

Send me an email if you have any questions: steinberk@gmx.de

### How to run it
*Remember to install dependencies via 'pip install -r requirements'*
 - `run.py` can be used as a quick start to create the dataset, train the models, and evaluate both
 This way it is not necessary to dive deep into the code.
 - `homotopynet/_presets` contains the dataset & trained models which were used for the thesis

### Different Modules
 - `routes.py` : classes for representing and generating constrained free spaces & routes
 - `preprocessing.py` : functions for preprocessing raw data according the needs of the models
 - `learning/` : modules for composing different nn-modules with existing architectures
 - `eval.py` : functions for collecting metrics & test results over models & datasets
 - `visualize.py` : functions for plotting various parts of the dataset
 - `test_maker.py` : functions for creating & storing testcases
