###How to use Presets:
 - copy datasets folder to homotopynet/
 - copy networks folder to homotopynet/_data/shelves

Reminder: These models only run with the dataset they were trained on.