import os, shutil
import json
import numpy as np
from routes import complex_room, gen_samples
import matplotlib.pyplot as plt
import random

from PIL import Image
#Location for storing data
path_dataset = "dataset/"

#Locations for storing maps
path_maps = "dataset/maps/"
path_maps_raw = path_maps+"raw/"
path_maps_prep = path_maps+"preprocessed/"
path_maps_aug = path_maps_prep+"augmented/"

#Locations for storing routes
path_routes = "dataset/routes/"
path_routes_raw = path_routes+"raw/"
path_routes_prep = path_routes+"preprocessed/"
path_routes_aug = path_routes_prep+"augmented/"


def gen_data():
    """Generates Route-Pair samples for each existing bitmap """
    for map_path in os.listdir(path_maps_raw):
        if map_path.endswith(".bmp"):
            map = map_path[:-4]
            map_data = {"routes": [], "homotopic": [], "alternative" : []}
            print("Processing Map:",map+".bmp")
            for _ in range(5):
                penalty_samples = gen_samples(map+".bmp", 15, algorithm="penalty")
                onep_samples = gen_samples(map+".bmp", 15, algorithm="one_patching")
                for samples in (onep_samples,): #penalty_samples
                    ln = len(map_data["routes"])
                    map_data["routes"] += samples["routes"]
                    map_data["homotopic"] += [(r1+ln,r2+ln) for r1, r2 in samples["homotopic"]]
                    map_data["alternative"] += [(r1+ln,r2+ln) for r1, r2 in samples["alternative"]]
            with open(path_routes_raw+map+".json", "w") as data_file:
                #print("Amount :",len(map_data["routes"]),max(map_data["homotopic"]))
                json.dump(map_data, data_file)

def _rotate_routes(route1, route2):
    from collections import deque
    route_cat = deque(route1 + list(reversed(route2[1:-1])))
    route_cat.rotate(int(random.random()*len(route_cat)))

    new_route_cat = list(route_cat)
    rand_val = int(random.random()*len(route_cat))
    new_route1 = new_route_cat[:rand_val]
    new_route2 = new_route1[:1] + list(reversed(new_route_cat[rand_val:])) + new_route1[-1:]

    return new_route1, new_route2

def supplement_homotopics():
    "Add additional homotopic route pairs, while less then 40% are homotopic"
    for file_path in os.listdir(path_routes_raw):
        if file_path.endswith(".json"):
            file_name = file_path[:-5]
            #print("Map name:", file_name)
            with open(path_routes_raw+file_name+".json", "r") as route_file:
                route_data = json.load(route_file)
            routes = route_data["routes"]
            homotopics = route_data["homotopic"]
            alternatives = route_data["alternative"]
            while len(homotopics)/(len(homotopics)+len(alternatives)) <= 0.40 and len(homotopics)>0:
                new_routes = list()
                for r1, r2 in homotopics:
                    new_route1, new_route2 = _rotate_routes(routes[r1], routes[r2])
                    new_routes.append((new_route1, new_route2))
                for nr1, nr2 in new_routes:
                    index_1 = len(routes)
                    routes.append(nr1)
                    routes.append(nr2)
                    homotopics.append([index_1, index_1+1])
            #print(len(homotopics)/(len(homotopics)+len(alternatives)))
            with open(path_routes_prep+file_name+".json", "w") as route_file:
                route_data["routes"] = routes
                route_data["homotopic"] = homotopics
                route_data["alternative"] = alternatives
                json.dump(route_data, route_file)


def stretch_images():
    "Stretches data according to shape of biggest map"
    images = list()
    x_max, y_max = 0,0
    for file_path in os.listdir(path_maps_raw):
        if file_path.endswith(".bmp"):
            img = Image.open(path_maps_raw+file_path)
            images.append((img, file_path))
            x, y = img.size
            x_max = max(x, x_max)
            y_max = max(y, y_max)
    x_max = y_max = max(x_max,y_max)
    for img, path in images:
        x_fac, y_fac = x_max/img.size[1], y_max/img.size[0]
        img_resized = img.resize((x_max, y_max))
        img_resized.save(path_maps_prep + path)

        if path[:-4]+".json" in os.listdir(path_routes_prep):
            with open(path_routes_prep + path[:-4] + ".json", 'r') as file:
                data = json.load(file)
            routes = list()
            ###WiP Stretching Routes
            for route in data["routes"]:
                stretched_route = list()
                for row, col in route:
                    stretched_route.append((int(row * x_fac), int(col * y_fac)))
                routes.append(stretched_route)

            data["routes"] = routes
            with open(path_routes_prep + path[:-4] + ".json", 'w+') as file:
                json.dump(data, file)


def rotate_images():
    "Rotate images & routes by 90, 180, 270°"
    for file_path in os.listdir(path_routes_prep):
        if file_path.endswith(".json"):
            file_name = file_path[:-5]
            with open(path_routes_prep+file_name+".json", "r") as route_file:
                route_data = json.load(route_file)
            a = map_size()
            ## Rotate 90°
            routes_90 = list()
            for route in route_data["routes"]:
                new_route = list()
                for row,col in route:
                    new_route.append((col,a-1-row))
                routes_90.append(new_route)
            img = Image.open(path_maps_prep+file_name+".bmp")
            img = img.rotate(-90)
            img.save(path_maps_prep+file_name+"_90"+".bmp")
            with open(path_routes_prep+file_name+"_90"+".json", "w+") as route_file:
                route_data["routes"] = routes_90
                json.dump(route_data, route_file)

            ## Rotate 180°
            routes_180 = list()
            for route in routes_90:
                new_route = list()
                for row,col in route:
                    new_route.append((col,a-1-row))
                routes_180.append(new_route)
            img = img.rotate(-90)
            img.save(path_maps_prep+file_name+"_180"+".bmp")
            with open(path_routes_prep+file_name+"_180"+".json", "w+") as route_file:
                route_data["routes"] = routes_180
                json.dump(route_data, route_file)


            ## Rotate 270°
            routes_270 = list()
            for route in routes_180:
                new_route = list()
                for row,col in route:
                    new_route.append((col,a-1-row))
                routes_270.append(new_route)
            img = img.rotate(-90)
            img.save(path_maps_prep+file_name+"_270"+".bmp")
            with open(path_routes_prep+file_name+"_270"+".json", "w+") as route_file:
                route_data["routes"] = routes_270
                json.dump(route_data, route_file)

def map_size(map_name = "difmap"):
    """Return the size of some preprocessed map"""
    img = Image.open(path_maps_prep + map_name + ".bmp")
    return img.size[0]

def scale_routes():
    """Scales Routes to value range [0,1]"""
    side_len = map_size()
    total_len = side_len*side_len
    for file_path in os.listdir(path_routes_prep):
        if file_path.endswith(".json"):
            with open(path_routes_prep + file_path) as file:
                data = json.load(file)
            routes = list()
            for route in data["routes"]:
                routes.append([(x * side_len + y)/(total_len + 2) for x,y in route])
            data["routes"] = routes
            with open(path_routes_prep+file_path, "w") as file:
                json.dump(data, file)

def _len_max(prep = False):
    """Return maximum route length"""
    if prep:
        route_path = path_routes_prep
    else:
        route_path = path_routes_raw
    maxlen = 0
    for file_path in os.listdir(route_path):
        if file_path.endswith(".json"):
            with open(route_path + file_path) as file:
                data = json.load(file)
                maxlen = max(maxlen, max([len(r) for r in data["routes"]]))
    return maxlen

def _len_avg():
    """Return avg length of a route"""
    total_len = 0
    n = 0
    for file_path in os.listdir(path_routes_raw):
        if file_path.endswith(".json"):
            with open(path_routes_raw+file_path) as file:
                data = json.load(file)
                for route in data["routes"]:
                    n += 1
                    total_len += len(route)
    return total_len/n

def equalize_route(route, maxlen):
    """Maxlen embedding: Stretch route to maxlen by appending NaN values"""
    nan = [(map_size()*map_size() + 2)/(map_size()*map_size() +2)]
    f = int((maxlen-len(route))/2)
    l = ((maxlen-len(route)) - f)
    return  f*nan + route + l*nan

def equalize_routes():
    """Apply maxlen embedding to all routes"""
    maxlen = _len_max(prep=True)

    for file_path in os.listdir(path_routes_prep):
        if file_path.endswith(".json"):
            with open(path_routes_prep+file_path) as file:
                data = json.load(file)
                routes = data["routes"]
                for i in range(len(routes)):
                    routes[i] = equalize_route(routes[i], maxlen)
            with open(path_routes_prep+file_path, "w") as file:
                json.dump(data, file)

def move_augmented():
    """Move Rotate routes & maps to seperate folder"""
    for map_file in os.listdir(path_maps_prep):
        if map_file.endswith(".bmp"):
            map_name = map_file[:-4]
            route_file = map_name+".json"
            if map_name.endswith("90") or map_name.endswith("180") or map_name.endswith("270"):
                shutil.move(path_maps_prep+map_file, path_maps_aug+map_file)
                shutil.move(path_routes_prep+route_file, path_routes_aug+route_file)

def store_routes():
    """Store & aggregate routes to hdf5-dataset"""
    import h5py
    f = h5py.File(path_dataset+"dataset.hdf5", "w")
    map_group = f.create_group("maps")
    route_group = f.create_group("routes") #rg

    #Train Set
    rg_test = f.create_group("test")
    rg_test_unknown = rg_test.create_group("unknown")
    rg_test_known = rg_test.create_group("known")

    #Test Set
    rg_train = f.create_group("train")
    rg_train_unknown = rg_train.create_group("unknown")
    rg_train_known = rg_train.create_group("known")

    for fileNo, file_path in enumerate(os.listdir(path_routes_prep)):
        train_amount = 0.7
        train_select = lambda li : [k for i, k in enumerate(li) if i <= int(len(li)*train_amount)]
        test_select = lambda li : [k for i, k in enumerate(li) if i <= int(len(li)*(1-train_amount))]

        if file_path.endswith(".json"):
            map_names = [file_path[:-5] + suffix for suffix in ["","_90","_180","_270"]]
            for map_name in map_names:
                if map_name.endswith("_90") or map_name.endswith("_180") or map_name.endswith("_270"):
                    map_path = path_maps_aug + map_name +".bmp"
                    route_path = path_routes_aug + map_name +".json"
                else:
                    map_path = path_maps_prep+map_name +".bmp"
                    route_path = path_routes_prep+map_name +".json"

                with open(route_path, 'r') as file:
                    route_data = json.load(file)
                cor = complex_room().from_file(map_path)
                map_group.create_dataset(map_name, data=cor.get_room())

                kr_train = rg_train_known.create_group(map_name)
                ur_train = rg_train_unknown.create_group(map_name)

                kr_test = rg_test_known.create_group(map_name)
                ur_test = rg_test_unknown.create_group(map_name)

                route_group.create_dataset(map_name, data=route_data["routes"])

                #70% Train Data, 30% Test Data
                if fileNo <= int(train_amount*len(os.listdir(path_routes_prep))):
                    kr_train.create_dataset("homotopic", data=train_select(route_data["homotopic"]))
                    kr_train.create_dataset("alternative", data=train_select(route_data["alternative"]))
                    kr_test.create_dataset("homotopic", data=test_select(route_data["homotopic"]))
                    kr_test.create_dataset("alternative", data=test_select(route_data["alternative"]))

                    ur_train.create_dataset("homotopic", data=route_data["homotopic"])
                    ur_train.create_dataset("alternative", data=route_data["alternative"])

                else:
                    kr_train.create_dataset("homotopic", data=train_select(route_data["homotopic"]))
                    kr_train.create_dataset("alternative", data=train_select(route_data["alternative"]))
                    kr_test.create_dataset("homotopic", data=test_select(route_data["homotopic"]))
                    kr_test.create_dataset("alternative", data=test_select(route_data["alternative"]))

                    ur_test.create_dataset("homotopic", data=route_data["homotopic"])
                    ur_test.create_dataset("alternative", data=route_data["alternative"])

    f.close()


def complete_preprocessing():
    supplement_homotopics()
    stretch_images()
    rotate_images()
    scale_routes()
    equalize_routes()
    move_augmented()

def gen_dataset():
    gen_data()
    complete_preprocessing()
    store_routes()

# Routen mit Trennzeichen trennen
def separate_routes():
    """Concatenates 2 Routes, but adds seperator value in the middle"""
    separator = [(map_size()*map_size() + 2)/(map_size()*map_size()+2)]
    for file_path in os.listdir(path_routes_prep):
        if file_path.endswith(".json"):
            with open(path_routes_prep + file_path) as file:
                data = json.load(file)
                routes = data["routes"]
            homotopic = list()
            for r1, r2 in data["homotopic"]:
                try:
                    homotopic.append(routes[r1] + separator + routes[r2])
                except:
                    print("Append didn't work", r1, r2)
            alternative = list()
            for r1, r2 in data["alternative"]:
                alternative.append(routes[r1] + separator + routes[r2])

            with open(path_routes_prep + file_path, "w") as file:
                prep_data = dict()
                prep_data["homotopic"] = homotopic
                prep_data["alternative"] = alternative
                json.dump(prep_data, file)

def preprocess_route_sample(route1, route2, length, cat=False):
    """Apply maxlen Embedding to Route Pair"""
    side_len = map_size()
    total_len = side_len*side_len
    route1=[(x * side_len + y)/(total_len + 2) for x,y in route1]
    route2=[(x * side_len + y)/(total_len + 2) for x,y in route2]

    if cat:
        route_cat = route1 + list(reversed(route2))[1:]
        route_cat = equalize_route(route_cat, length*2)
        return route_cat
    else:
        route1= equalize_route(route1, length)
        route2= equalize_route(route2, length)
        return (route1, route2)

def normalize_route_values(route, slen=100, add=2, batch=False):
    """Takes a preprocessed route and normalizes it's value range"""
    import math
    if batch:
        normalized_value_list = list()
        for r in route:
            normalized_values = normalize_route_values(r.squeeze(), slen, add, batch=False)
            normalized_value_list.append(torch.Tensor(normalized_values))
        return torch.stack(normalized_value_list)
    else:
        normalized_values = route*(slen*slen+add)
        return [math.ceil(z.item()) for z in normalized_values if z < (slen*slen)]

def normalize_route(route, slen=100, add=2):
    """Takes a preprocessed route and hands you a useable one"""
    tuple_form = [(int(z/slen), int(z%slen)) for z in normalize_route_values(route,slen,add)]
    return tuple_form
