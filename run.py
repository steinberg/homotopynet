import preprocessing as prep
import eval as ev
import learn
import test_maker as test

import sys,os
def __block_print():
    """Suppresses prints to console.\n
    If you're not interested in logs, call this before executing other functions."""
    sys.stdout = open(os.devnull, 'w')
def __enable_print():
    """Enable prints to console again."""
    sys.stdout = sys.__stdout__



def gen_dataset():
    """Generates Indoor-Routes-Dataset, from all raw bitmaps in 'dataset/maps/raw',
    by generating route-pairs, homotopy-labels and applying various preprocessing steps.\n

    The dataset is then stored in 'dataset/dataset.hdf5'.
    Old dataset will be overwritten if it exists."""
    prep.gen_dataset()

def train_all_models(models_to_train=None, train_known=True, train_unknown=True):
    """Trains all given models on the existing datasets. \n
    By default all models inspected in the bachelor's thesis will be trained, which might take up to 4 days.\n \n

    ---Parameters:\n
    models_to_train : dict() in the form of {(embedding type):{(model name):Model_Constructor}}\n
    train_known : If True models are trained on Dataset A \n
    train_unknown : If True models are trained on Dataset B"""
    learn.train_all_models(models_to_train, train_known, train_unknown)

def eval_dataset():
    """Collects metrics about the distribution of maps and routes in the dataset\n
    Metrics can be stored to csv with metrics_to_csv()"""
    ev.run_dataset_eval()

def eval_all_models():
    """Collects metrics about model performance and applies testcases for all models\n
    Metrics can be stored to csv with metrics_to_csv()\n
    Test results are stored visually at 'eval/(dataset type)/(model name)/test/'\n

    New testcases can be created using draw_testcase()"""
    ev.run_model_eval()

def metrics_to_csv():
    """Stores collected metrics about dataset & models to .csv files\n
    Call this function after both have been evaluated. \n\n

    Directories for Storing results:\n
    Overall metrics        -> 'eval/(dataset kind)/metrics.csv'\n
    Map specific metrics   -> 'eval/(dataset kind)/map_metrics.csv'\n"""
    ev.metrics_to_csv()

def draw_testcase(log=False):
    """Create a test case by drawing routes on a map.\n
    Use any 100x100 bitmap by saving the raw map to 'dataset/maps/eval/(map name).bmp'\n
    Then RGB-color 'cfcfcf' for drawing route pairs on this map.\n\n

    Each route needs to be drawn and stored in a seperate file:\n
    Test route 1  -> 'dataset/maps/eval/routes/(map name)1.bmp'\n
    Test route 2  -> 'dataset/maps/eval/routes/(map name)2.bmp'\n\n

    ---Parameters:\n
    log : If True plots route pair & logs results after creation"""
    test.make_test_case(log=log)
