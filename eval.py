import torch
import os

from torch.utils.data import DataLoader, Dataset
import sys
sys.path.append("learning/networks")

from learning.datasets import *
from learning.networks.MainNetwork import *
from learning.func import *
import preprocessing as prep

import time
import random
import shelve, json, h5py, csv
from visualize import plt_route, plt_indices, plt_route_list
from routes import complex_room, bit_graph, check_set_homotopy

from sklearn.metrics import *

import matplotlib.pyplot as plt
import seaborn as sns

#Locations for storing maps
path_maps_raw = "dataset/maps/raw/"
path_maps_prep = "dataset/maps/preprocessed/"
path_maps_aug = "dataset/maps/preprocessed/augmented/"

#Locations for storing routes
path_routes_raw = "dataset/routes/raw/"
path_routes_prep = "dataset/routes/preprocessed/"
path_routes_aug = "routes/preprocessed/augmented/"

#Location for storing results
path_eval = "eval/"

id_to_tag = {
        0 : "alternative",
        1 : "homotopic"
        }

test_sets = {"known":IndoorRoutesTestSetA,"unknown":IndoorRoutesTestSetB}

def test_model(model, net_name, kind):
    """Tests model on all created test scenarios\n Results are stored visually"""
    test_path = "_data/shelves/test/"

    dsq = lambda l: l.unsqueeze(0).unsqueeze(0)
    route_len = model.route_len
    for test_name in os.listdir(test_path):
        if test_name.endswith(".bak"):
            test = shelve.open(test_path + test_name[:-4])
            test_name=test_name[:-4]
            for map_name in test.keys():
                try:
                    os.mkdir(fp + map_name)
                except Exception as e:
                    #print(fp + map_name, e)
                    pass
                try:
                    bitmap, (r1,r2) = test[map_name]
                    if kind=="concat":
                        route = prep.preprocess_route_sample(r1,r2,length=route_len, cat=True)
                        pred=model(bitmap, route)
                    elif kind=="individual":
                        rt1, rt2 = prep.preprocess_route_sample(r1,r2,length=route_len)
                        pred=model((torch.Tensor(bitmap).reshape(1,1,100,100), torch.Tensor(rt1).reshape(1,1,route_len), torch.Tensor(rt2).reshape(1,1,route_len))).argmax(1)
                    try:
                        os.mkdir(path_eval + net_name +"/test")
                    except:
                        pass
                    plt_route(bitmap, r1, r2, label=id_to_tag[pred.item()], fp=path_eval + net_name +"/test/"+ test_name + "_" + map_name +".png")
                    #plt_route(bitmap, r1, r2, label=id_to_tag[pred.item()], fp=fp + map_name +"/" +test_name +".png")
                    #TODO: Return png File
                except:
                    pass
            test.close()

def vis_metrics(all_labels, all_preds, test_set, model_name=None, fp=None):
    """Plots/Stores a batch of predictions, grouped by their type of error"""
    alt, hom = 0, 1
    z = torch.stack((all_labels.type(torch.float), all_preds.type(torch.float)))
    indices = {
        "False Homotopic" : [i for i in range(len(test_set)) if z[1,i].item()==hom and z[0,i].item()==alt],
        "False Alternative" : [i for i in range(len(test_set)) if z[1,i].item()==alt and z[0,i].item()==hom],
        "True Homotopic" : [i for i in range(len(test_set)) if z[1,i].item()==hom and z[0,i].item()==hom],
        "True Alternative" : [i for i in range(len(test_set)) if z[1,i].item()==alt and z[0,i].item()==alt]
        }
    for error in indices:
        if model_name or fp:
            if model_name:
                file_path=path_eval+model_name+"/"+error+".png"
            if fp:
                file_path = fp + "/" +error+".png"
            plt_indices(test_set, random.sample(indices[error], min(10,len(indices[error]))), label=error, fp=file_path)
        else:
            plt_indices(test_set, random.sample(indices[error], min(10,len(indices[error]))), label=error)

@torch.no_grad()
def get_map_accuracy(model,test_set_name, fp=None):
    map_accuracy = dict()
    D = h5py.File("dataset.hdf5",'r')
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            eval_map_name = route_path[:-5]
            if "homotopic" in D["test"][test_set_name][eval_map_name]:
                mapping = {"homotopic":1, "alternative":0}
                bitmap = torch.Tensor(D["maps"][eval_map_name]).unsqueeze(0)
                rts = D["routes"][eval_map_name]
                bitmaps, labels, routes1, routes2 = list(), list(), list(), list()
                for label in mapping:
                    for rid1, rid2 in D["test"][test_set_name][eval_map_name][label]:
                        route1, route2 = torch.Tensor(rts[rid1]).unsqueeze(0), torch.Tensor(rts[rid2]).unsqueeze(0)
                        routes1.append(route1)
                        routes2.append(route2)
                        labels.append(mapping[label])
                        bitmaps.append(bitmap)
                bitmaps, routes1, routes2 = torch.stack(bitmaps), torch.stack(routes1), torch.stack(routes2)
                labels = torch.Tensor(labels)
                preds = model((bitmaps, routes1, routes2)).argmax(dim=1)
                correct = get_num_correct(preds, labels)
                map_accuracy[eval_map_name] = correct/len(labels)
                #print(eval_map_name, correct/len(labels))
                #Store sample visualizations of errors
                #if fp:
                #    try:
                #        os.mkdir(fp +"/" + eval_map_name)
                #    except:
                #        pass
                #    fp = fp +"/" + eval_map_name
                #    dataset = ((bitmaps, routes1, routes2), None)
                #    vis_metrics(labels, preds, dataset, fp)

    D.close()

    return map_accuracy

def store_metrics(all_labels, all_preds, test_time, net_name, map_accuracy):
    #Todo: store as csv
    correct = get_num_correct(all_preds, all_labels)
    F = shelve.open(path_eval + net_name + "/" + "metrics")
    F["accuracy"] = correct/len(all_labels)
    F["test_time"] = test_time
    F["Precision_Alt"] = precision_score(all_labels.numpy(), all_preds.numpy(), pos_label=1)
    F["Precision_Hom"] = precision_score(all_labels.numpy(), all_preds.numpy(), pos_label=0)

    F["Recall_Alt"] = recall_score(all_labels.numpy(), all_preds.numpy(), pos_label=1)
    F["Recall_Hom"] = recall_score(all_labels.numpy(), all_preds.numpy(), pos_label=0)

    F["F1_Alt"] = (2*F["Precision_Alt"]*F["Recall_Alt"])/(F["Precision_Alt"]+F["Recall_Alt"])
    F["F1_Hom"] = (2*F["Precision_Hom"]*F["Recall_Hom"])/(F["Precision_Hom"]+F["Recall_Hom"])

    F["map_accuracy"] = map_accuracy

    F.close()


    print("Total Correct", correct, "Total", len(all_labels), "Accuracy", correct/len(all_labels))


def eval_model(network, test_set, net_name, test_set_name):
    test_loader = DataLoader(test_set, batch_size=100, shuffle=False)
    (_,rt,_),_ = next(iter(test_loader))
    start_time = time.perf_counter()
    all_preds, all_labels = get_all_preds(network, test_loader)
    end_time = time.perf_counter()
    test_time = end_time - start_time
    try:
        os.mkdir(path_eval+test_set_name+"/"+net_name+"/maps")
    except:
        pass
    map_accuracy = get_map_accuracy(network, test_set_name, fp=path_eval+test_set_name+"/"+net_name+"/maps")

    vis_metrics(all_labels, all_preds, test_set, model_name=net_name)
    store_metrics(all_labels, all_preds, test_time, net_name, map_accuracy)
    test_model(network, net_name, kind="individual", route_len=rt.shape[-1])

    return all_preds, all_labels
    print("Evaluated", net_name)




def compare_speed(models):
    pass

def eval_learning(model, name, rt_type):
    dir = path_eval + name
    try:
        os.mkdir(dir)
    except Exception as e:
        #print(e)
        pass

    #test_model(model, dir+"/", rt_type)
    #eval_model(model, validation_set, name)
    #compare_speed(models)

############## Eval datasets
def concat_routes(rt1, rt2):
    return rt1 + list(reversed(rt2))[1:]

def overlaps_edges_per_len(map_name, plot=False):
    #Graph showing amount of space_per_len for one route
    #Graph showing routes for one map
    edges_per_len = []
    with open(path_routes_raw+map_name+".json") as route_file:
        route_data = json.load(route_file)
        routes = route_data["routes"]
        route_pairs = route_data["homotopic"] + route_data["alternative"]
        for rid1, rid2 in route_pairs:
            rt1, rt2 = [(r,c) for r,c in routes[rid1]], [(r,c) for r,c in routes[rid2]]
            concat = concat_routes(rt1,rt2)
            unique_edges = len({(r,c) for r,c in concat})
            overlap_edges = len({e for e in rt1 if e in rt2})
            edges_per_len.append(overlap_edges/unique_edges)
    rows,cols=[],[]
    for i in np.unique(edges_per_len):
        rows.append(i)
        cols.append(np.count_nonzero(np.array(edges_per_len)==i))
    if plot:
        sns.distplot(edges_per_len, hist=True,kde=True) #choose colors
        plt.xlim(left=0, right=1)
        plt.title(map_name)
        plt.xlabel(r"$\frac{\mathrm{shared\_edges}}{\mathrm{unique\_edges}}$")
        #plt.ylabel("count")
        plt.savefig(path_eval+"dataset/"+map_name+"/overlaps_edges_per_len.png")
        plt.clf()
    return edges_per_len

def overlaps_area_per_len(map_name, plot=False):
    #Graph showing amount of space_per_len for one route
    #Graph showing routes for one map
    space_per_len = []
    with open(path_routes_raw+map_name+".json") as route_file:
        route_data = json.load(route_file)
        routes = route_data["routes"]
        route_pairs = route_data["homotopic"] + route_data["alternative"]
        for rid1, rid2 in route_pairs:
            total_len = len(routes[rid1]) + len(routes[rid2])
            cor = complex_room.from_file(path_maps_raw +map_name+".bmp")
            overlap_area = cor.overlap_area(routes[rid1], routes[rid2])
            space_per_len.append(overlap_area/total_len)
    rows,cols=[],[]
    for i in np.unique(space_per_len):
        rows.append(i)
        cols.append(np.count_nonzero(np.array(space_per_len)==i))
    if plot:
        sns.distplot(space_per_len, hist=True,kde=True) #choose colors
        plt.xlim(left=0)
        plt.title(map_name)
        plt.xlabel(r"$\frac{\mathrm{area}}{\mathrm{pair\_length}}$")
        #plt.ylabel("count")
        plt.savefig(path_eval+"dataset/"+map_name+"/overlaps_area_per_len.png")
        plt.clf()
    return space_per_len

def lens_individual(map_name, plot=False):
    all_lens = list()
    with open(path_routes_raw+map_name+".json") as route_file:
        route_data = json.load(route_file)
    routes = route_data["routes"]
    for route in routes:
        all_lens.append(len(route))

    rows,cols=[],[]
    n_unique = len(np.unique(all_lens))
    for i in range(n_unique):
        rows.append(i)
        cols.append(np.count_nonzero(np.array(all_lens)==i))
    if plot:
        sns.distplot(all_lens, hist=True,kde=True) #choose colors
        plt.xlim(left=0, right=max(all_lens))
        #plt.plot([np.average(all_lens)]*n_unique, np.arange(n_unique))
        plt.title(map_name)
        plt.xlabel(r"route length")
        #plt.ylabel("count")
        plt.savefig(path_eval+"dataset/"+map_name+"/lens_individual.png")
        plt.clf()
    return all_lens

def lens_total(map_name, plot=False):
    all_lens = list()
    with open(path_routes_raw+map_name+".json") as route_file:
        route_data = json.load(route_file)
    routes = route_data["routes"]
    route_pairs = route_data["homotopic"] + route_data["alternative"]
    for rid1, rid2 in route_pairs:
        all_lens.append(len(concat_routes(routes[rid1], routes[rid2])))
    rows,cols=[],[]
    n_unique = len(np.unique(all_lens))
    for i in range(n_unique):
        rows.append(i)
        cols.append(np.count_nonzero(np.array(all_lens)==i))
    if plot:
        sns.distplot(all_lens,hist=True,kde=True) #choose colors
        plt.xlim(left=0, right=max(all_lens))
        #plt.plot([np.average(all_lens)]*n_unique, np.arange(n_unique))
        plt.title(map_name)
        plt.xlabel(r"route pair length")
        #plt.ylabel("count")
        plt.savefig(path_eval+"dataset/"+map_name+"/lens_pairs.png")
        plt.clf()
    return all_lens

def obstacle_ratio(map_name):
    cor = complex_room.from_file(path_maps_raw +map_name+".bmp")
    return np.count_nonzero(cor.get_room() == cor.OBS)/np.product(cor.get_room().shape)

def num_homotopy_classes(routes, map_name):
    cor = complex_room.from_file(path_maps_raw+map_name+".bmp")
    n_classes = 0
    while routes:
        rt1 = routes[0]
        n_classes += 1
        to_delete=[]
        for i,rt2 in enumerate(routes):
            if cor.check_homotopy_simple(rt1,rt2) == cor.HOMO:
                to_delete.append(i)
        #delete_list = [routes[rid] for rid in to_delete]
        #plt_route_list(cor.get_room(),delete_list, label="Homotopieklasse")
        for i in sorted(to_delete, reverse=True):
            del routes[i]
    return n_classes

def found_homotopy_classes(map_name, plot=False):
    all_n_classes = []
    with open(path_routes_raw+map_name+".json") as route_file:
        route_data = json.load(route_file)
    tuple_form = lambda l : [(r,c) for r,c in l]
    routes = [tuple_form(rt) for rt in route_data["routes"]]
    for rt in routes:
        n_classes = 0
        rel_indexes = [i for i,al in enumerate(routes) if al[0] == rt[0] and al[-1] == rt[-1]]
        relatives = [routes[i] for i in rel_indexes]
        for i in sorted(rel_indexes, reverse=True):
            del routes[i]
        all_n_classes.append(num_homotopy_classes(relatives,map_name))
    if plot:
        #plot homotopymap
        pass
    return all_n_classes

def all_homotopy_classes(map_name):
    cor = complex_room.from_file(path_maps_raw+map_name+".bmp")
    bm = cor.get_room()
    bg = bit_graph(cor)
    all_routes = list()
    homotopy_classes = 0
    for _ in range(4):
        start, goal = bg.get_random_vertices()
        routes = cor.penalty(bg, start, goal, limit=400) + cor.one_patching(bg, start, goal, limit=400)
        all_routes += routes
        homotopy_classes = max(homotopy_classes, num_homotopy_classes(routes, map_name))
    #plt_route_list(bm, all_routes)
    return homotopy_classes

def covered_homotopy_classes(map_name):
    return max(found_homotopy_classes(map_name))/all_homotopy_classes(map_name)

def class_distribution():
    pass

def plt_all_routes():
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            try:
                map_name = route_path[:-5]
                cor = complex_room.from_file(path_maps_raw + map_name+".bmp")
                with open(path_routes_raw+map_name+".json") as route_file:
                    route_data = json.load(route_file)
                routes = route_data["routes"]
                plt_route_list(cor.get_room(),routes,map_name, True, fp=path_eval+"dataset/"+map_name+"/"+map_name+".png")
            except:
                pass

def plt_labeled_samples():
    """For each map plot one sample of each class"""
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            try:
                map_name = route_path[:-5]
                cor = complex_room.from_file(path_maps_raw + map_name+".bmp")
                with open(path_routes_raw+map_name+".json") as route_file:
                    route_data = json.load(route_file)
                routes = route_data["routes"]
                hid1, hid2 = random.choice(route_data["homotopic"])
                aid1, aid2 = random.choice(route_data["alternative"])
                plt_route(cor.get_room(), routes[hid1], routes[hid2], label="", endings=True, fp=path_eval+"dataset/"+map_name+"/homotopic.png")
                plt_route(cor.get_room(), routes[aid1], routes[aid2], label="", endings=True, fp=path_eval+"dataset/"+map_name+"/alternative.png")
            except Exception as e:
                #print(e)
                pass


def amount_per_map(metric_function, label="",id="", plot=False, ylim=None):
    maps = []
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            maps.append(route_path[:-5])
    #graph showing variance over all routes
    metrics = list()
    for map_name in maps:
        metrics.append(metric_function(map_name))
    if plot:
        x = np.arange(len(metrics))
        plt.title(label)
        plt.bar(x , metrics)
        plt.xticks(x, x, rotation=45)
        plt.xlabel("Map Id")
        if ylim:
            plt.ylim(top = ylim)
        plt.savefig(path_eval+"dataset/"+id+".png")
        plt.clf()
    return metrics

def var_per_map(metric_function, label="", id="", plot=False):
    maps = []
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            maps.append(route_path[:-5])
    #graph showing variance over all routes
    metrics = list()
    for map_name in maps:
        metrics.append(np.sqrt(np.var(metric_function(map_name, plot=plot))))
    if plot:
        x = np.arange(len(metrics))
        plt.title(label)
        plt.bar(x , metrics)
        plt.ylabel("$\sqrt{\text{Var}}$")
        plt.xlabel("Map Id")
        plt.xticks(x, x, rotation=45)
        plt.savefig(path_eval+"dataset/"+id+".png")
        plt.clf()
    return metrics

def store_map_metrics(maps, metrics_per_map):
    with shelve.open(path_eval+"dataset/map_metrics") as metric_store:
        for i, map_name in enumerate(maps):
            map_metrics = {metric : metrics_per_map[metric][i] for metric in metrics_per_map}
            metric_store[map_name] = map_metrics

def eval_dataset():
    maps = []
    for route_path in os.listdir(path_routes_raw):
        if route_path.endswith(".json"):
            maps.append(route_path[:-5])
    for map_name in maps:
        try:
            os.mkdir(path_eval+"dataset/" + map_name)
        except Exception as e:
            #print(path_eval+"dataset/" + map_name, e)
            pass

    plt_all_routes()

    map_metrics = dict()
    #Sharing
    map_metrics["obstacle_ratio"] = amount_per_map(obstacle_ratio,label="Obstacle_Ratio", id="obstacle_ratio", plot=True)
    map_metrics["var_overlap_area"] = var_per_map(overlaps_area_per_len, label="Area per Length", id="var_overlap_area", plot=True)
    map_metrics["var_overlap_edges"] = var_per_map(overlaps_edges_per_len, label="Shared Edges per Unique Egdes)", id="var_overlap_edges", plot=True)
    #Stretch
    map_metrics["var_individual_len"] = var_per_map(lens_individual, label="Path Length", id="var_route_len", plot=True)
    map_metrics["var_pair_len"] = var_per_map(lens_total, label="Pair Length", id="var_pair_len", plot=True)
    #Descision Edges
    map_metrics["homotopy_classes_coverage"] = amount_per_map(covered_homotopy_classes, label="Homotopy Classes (Coverage)", id="coverage_homotopy_classes", plot=True, ylim=1)
    #Map Variance
    map_metrics["all_homotopy_classes"] = amount_per_map(all_homotopy_classes, label="Homotopy Classes", id="total_homotopy_classes", plot=True)

    store_map_metrics(maps, map_metrics)


def run_model_eval():
    for test_set_name in test_sets:
        print("Model eval for", test_set_name)
        global path_eval
        path_eval = path_eval+test_set_name+"/"
        test_set = test_sets[test_set_name]("individual")
        roc_data = []
        for net_path in os.listdir("_data/shelves/networks/"+test_set_name+"/"):
            if net_path.endswith(".bak"):
                net_name = net_path[:-4]
                try:
                    os.mkdir(path_eval+net_name)
                except Exception as e:
                    #print(path_eval+net_name, e)
                    pass
                with shelve.open("_data/shelves/networks/"+test_set_name+"/"+net_name) as F:
                    network = F[test_set_name+"_lr0.00015_bs100_eps20"]
                #try:
                preds, labels = eval_model(network, test_set, net_name, test_set_name)
                print("preds",preds)
                print("labels",labels)
                #except Exception as e:
                #    print(e)
                #    print(net_name, "_data/shelves/networks/"+test_set_name+"/"+net_name)
                # plot the roc curve for the model
                ns_fpr, ns_tpr, _ = roc_curve(labels.numpy(), preds.numpy())
                roc_data.append((net_name, (ns_fpr,ns_tpr)))
        for net_name, (ns_fpr, ns_tpr) in roc_data:
            plt.plot(ns_fpr, ns_tpr, marker=".", label=net_name)
        # axis labels
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        # show the legentd
        plt.legend()
        # show the plot
        plt.savefig(path_eval+"ROC.png")

def run_dataset_eval():
    global path_eval
    path_eval = "eval/"
    eval_dataset()
    path_eval = "eval/"

def metrics_to_csv():
    rows = list()
    for train_name in ["unknown", "known"]:
        with shelve.open("eval/dataset/map_metrics") as a:
            #Write Map specific metrics
            map_names = list(a.keys())
            metrics = list(a[map_names[0]])
            net_names = [net_name for net_name in os.listdir(path_eval+train_name) if os.path.isdir(path_eval+train_name+"/"+net_name)]
            for map_name in map_names:
                row = [map_name]
                for metric in metrics:
                        row.append(a[map_name][metric])
                for net_name in net_names:
                        with shelve.open(path_eval+train_name+"/"+net_name+"/metrics") as b:
                                if map_name in b["map_accuracy"]:
                                    row.append(b["map_accuracy"][map_name])
                rows.append(row)

        with open(path_eval+train_name+"/map_metrics.csv", 'w', newline='') as csvfile:
            metricwriter = csv.writer(csvfile, delimiter=',')
            metricwriter.writerow(["Map Name"] + metrics + net_names)
            for row in rows:
                metricwriter.writerow(row)
        #Write Overall Metrics
        rows = list()
        help_row = list()
        for net_name in net_names:
            row = [net_name]
            help_row = []
            with shelve.open(path_eval+train_name+"/"+net_name+"/metrics") as b:
                    for metric in b:
                        if not metric.endswith("map_accuracy"):
                            row.append(b[metric])
                            help_row.append(metric)
            rows.append(row)
        with open(path_eval+train_name+"/metrics"+'.csv', 'w', newline='') as csvfile:
            metricwriter = csv.writer(csvfile, delimiter=',')
            metricwriter.writerow(["Model Name"]+help_row)
            for row in rows:
                metricwriter.writerow(row)

@torch.no_grad()
def algorithm_test_time():
    time_store = dict()
    #Load all Models
    test_nets = {"RNN_Siamese":MainNetworkRNNSiamese()}
    for net_path in os.listdir("_data/shelves/networks/unknown"):
        if net_path.endswith(".bak"):
            net_name = net_path[:-4]
            with shelve.open("_data/shelves/networks/unknown/"+net_name) as F:
                network = F["unknown_lr0.00015_bs100_eps20"]
                test_nets[net_name] = network
    for net_name in test_nets:
        time_store[net_name] = list()
    time_store["Polygon_Method"] = list()

    for _ in range(500):
        #Load Random Map and Paths
        map_path = random.choice(os.listdir(path_maps_prep))
        while not map_path.endswith(".bmp"):
            map_path = random.choice(os.listdir(path_maps_prep))
        map_name = map_path[:-4]
        #Transform to Graph / Preprocessing
        cor = complex_room.from_file(path_maps_prep + map_name+".bmp")
        with open(path_routes_pred+map_name+".json") as route_file:
            route_data = json.load(route_file)
        routes = route_data["routes"]
        rid1, rid2 = random.choice(route_data["homotopic"])
        rt1, rt2 = torch.Tensor(routes[rid1]), torch.Tensor(routes[rid2])
        #Prepare Data
        for net_name in test_nets:
            input = (torch.Tensor(cor.get_room()).reshape(1,1,100,100), rt1.reshape(1,1,867), rt2.reshape(1,1,867))
            model = test_nets[net_name]
            start_time = time.perf_counter()
            model(input)
            end_time = time.perf_counter()
            time_store[net_name].append(end_time - start_time)
        #Note Time while Tesing
        rt1,rt2 = prep.normalize_route(rt1), prep.normalize_route(rt2)
        start_time = time.perf_counter()
        cor.fill_space_simple(rt1,rt2)
        end_time = time.perf_counter()
        time_store["Polygon_Method"].append(end_time - start_time)

    for test_name in time_store:
        print(test_name, np.average(time_store[test_name]), np.median(time_store[test_name]))




@torch.no_grad()
def algorithm_test_time_optim():
    time_store = dict()
    #Load all Models
    test_nets = {"RNN_Siamese":MainNetworkRNNSiamese(),"CNN_2Channel":MainNetworkCNN2Channel(),"CNN_Siamese":MainNetworkCNNSiamese(),"CSTS_Siamese":MainNetworkCSTSSiamese(),"Baseline":MainNetworkBaseline(),"MainNetworkCNN2Channel_MR":MainNetworkCNN2Channel_MR()}
    for net_name in test_nets:
        time_store[net_name] = list()
    time_store["Polygon_Method"] = list()

    for map_path in [path_maps_raw + suf for suf in ["difmap.bmp","Haus_A.bmp"]]:
        cor = complex_room.from_file(map_path)
        bg = bit_graph(cor)
        start,goal = bg.get_random_vertices()
        routes = cor.one_patching(bg,start,goal,limit=250)

        for net_name in test_nets:
            mps = torch.Tensor(cor.get_room()).reshape(1,1,100,100)
            rt1, _ = prep.preprocess_route_sample(routes[0], [], length=867)
            rt1 = torch.Tensor(rt1).reshape(1,1,867)
            model = test_nets[net_name]
            model.forward_first(mps,rt1)
            print("forward_first")

            for rt2 in routes[1:]:
                start_time = time.perf_counter()
                rt2, _ = prep.preprocess_route_sample(rt2, [], length=867)
                rt2 = torch.Tensor(rt2).reshape(1,1,867)
                model.forward_second(rt2)
                end_time = time.perf_counter()
                time_store[net_name].append(end_time - start_time)

        rt1 = routes[0]
        for rt2 in routes[1:]:
            rt2 = rt2
            start_time = time.perf_counter()
            cor.fill_space_simple(rt1,rt2)
            end_time = time.perf_counter()
            time_store["Polygon_Method"].append(end_time - start_time)

        for net_name in time_store:
            store = time_store[net_name]
            sum_store = [store[0]]
            for i,t in enumerate(store[1:]):
                t += sum_store[i]
                sum_store.append(t)

    for test_name in time_store:
        print(test_name, np.average(time_store[test_name]), np.median(time_store[test_name]))

def test_something():
    """for net_path in os.listdir("_data/shelves/networks"):
        if net_path.endswith(".bak"):
            net_name = net_path[:-4]
            with shelve.open("_data/shelves/networks/"+net_name) as F:
                network = F["unknown_lr0.0001_bs100_eps10"]"""
    from learning.networks.MainNetwork import MainNetworkBaseline

    test_nets = {"baseline":MainNetworkBaseline()}
    for net_path in os.listdir("_data/shelves/networks"):
        if net_path.endswith(".bak"):
            net_name = net_path[:-4]
            with shelve.open("_data/shelves/networks/"+net_name) as F:
                network = F["unknown_lr0.00015_bs100_eps20"]
                test_nets[net_name] = network
    for net_name in test_nets:
        print("Map Accuracy for", net_name)
        model = test_nets[net_name]
        get_map_accuracy(model, ["smallroom", "Maze_B", "LMU_Juristische_Fak"])
