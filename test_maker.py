import imageio
from scipy.sparse import lil_matrix
import numpy as np
import os, subprocess
import imageio
import shelve

import sys, os
sys.path.append("learning/networks")

import torch

from routes import complex_room
from visualize import plt_route
import preprocessing as prep
from learning.networks.MainNetwork import MainNetworkBaseline



class Route_Test():

    def __init__(self):
        pass

    def route_from_file(self, MAP_NAME):
        self.MAP_NAME = MAP_NAME
        self.FILE_PATH = "dataset/maps/eval/cases/"+self.MAP_NAME
        absolute_path = os.path.join(self.FILE_PATH)

        image = imageio.imread(absolute_path, as_gray=0)
        if len(image.shape)>2:
            self.raw_room = np.array(image[:,:,1])
        else:
            self.raw_room = np.matrix(image)
        self.raw_room = lil_matrix(self.raw_room%255)
        end_points=set()
        nonzero = [(self.raw_room.nonzero()[0][i], self.raw_room.nonzero()[1][i]) for i in range(self.raw_room.nnz)]

        for pos in nonzero:
            row, col = pos
            if len(self.get_neighbors((row,col), nonzero)) == 1:
                end_points.add((row,col))
        print(end_points)
        route = list()
        if end_points:
            current_pos = end_points.pop()
            route.append(current_pos)
            while len(self.get_neighbors(current_pos, nonzero))>0:
                neighbors = self.get_neighbors(current_pos,nonzero)
                for neig in neighbors:
                    if neig not in route:
                        route.append(neig)
                        current_pos = neig
                        break
                else:
                    if end_points:
                        current_pos = end_points.pop()
                        route.append(current_pos)
                    else:
                        break
                if current_pos in end_points:
                    end_points.remove(current_pos)
                    if end_points:
                        current_pos = end_points.pop()
                        route.append(current_pos)
        for pos in nonzero:
            if pos not in route:
                route.append(pos)
        #print("Returning route", len(route))
        return route


    def add_neighbor(self, local_neighbors, pos, nonzero):
        if pos in nonzero:
            local_neighbors.append(pos)

    def get_neighbors(self, pos, nonzero):
        row, col = pos
        local_neighbors = list()
        add_local_neighbor = lambda pos : self.add_neighbor(local_neighbors, pos, nonzero)
        neig_range = (-1,1)
        for y in neig_range:
            if (row + y) >= 0 and (row + y) < self.raw_room.shape[0]:
                add_local_neighbor((row+y,col))
        for x in neig_range:
            if (col + x) >= 0 and (col +x) < self.raw_room.shape[1]:
                add_local_neighbor((row,col+x))
        for y in (-1,1):
            for x in (-1,1):
                if (row + y >= 0 and row + y < self.raw_room.shape[0]) and (col + x >= 0 and col +x < self.raw_room.shape[1]):
                    add_local_neighbor((row+y,col+x))
        return local_neighbors


def test_route(map, net, r1, r2, name):
    r1, r2 = preprocess_route(r1), preprocess_route(r2)
    route = preprocess_routes(r1,r2)
    input_single = (map, (r1,r2))
    output = net(input_single)
    plt_route(map, r1, r2, label = "output " + name)

def store_test_case(map_name, test_name, r1, r2, log=False):
    print("log",log)
    mapping = {0:"alternative",1:"homotopic"}
    cor = complex_room.from_file("dataset/maps/eval/"+map_name+".bmp")
    test_nets = {}
    for net_path in os.listdir("_data/shelves/networks/unknown"):
        if net_path.endswith(".bak"):
            net_name = net_path[:-4]
            with shelve.open("_data/shelves/networks/unknown/"+net_name) as F:
                network = F["unknown_lr0.00015_bs100_eps20"]
                test_nets[net_name] = network
    for net_name in test_nets:
        model = test_nets[net_name]
        route_len = model.route_len
        rt1,rt2 = prep.preprocess_route_sample(r1,r2,length=867)
        pred = model((torch.Tensor(cor.get_room()).reshape(1,1,100,100), torch.Tensor(rt1).reshape(1,1,route_len), torch.Tensor(rt2).reshape(1,1,route_len))).argmax(1)
        if log:
            print(net_name, mapping[pred.item()])
    if log:
        plt_route(cor.get_room(), r1, r2)
        test_file = shelve.open("_data/shelves/test/"+test_name, "c")
        test_file[map_name] = (cor.get_room(), (r1,r2))
        test_file.close()



def make_test_case(log=False):
    map_name = input("What's the name of the map?\n")
    print("Use RGB-color 'cfcfcf' for drawing. \n Save map to /dataset/maps/eval/ as ",map_name+".bmp\n", "Save test routes to /dataset/maps/eval/cases/ as", map_name+"1.bmp", "and", map_name+"2.bmp")
    map_abspath = 'C:\\Users\\Kilian\\Desktop\\homotopynet\\Routes-test\\dataset\\maps\\eval\\'+map_name+'.bmp'
    #subprocess.Popen(['C:\\Program Files\\Adobe\\Adobe Photoshop CC 2014\\Photoshop.exe', map_abspath])
    input("\nDone?\n")
    rt = Route_Test()
    r1 = rt.route_from_file(map_name+"1.bmp")
    r2 = rt.route_from_file(map_name+"2.bmp")
    test_name = input("\nWhat's the name of the test case?\n")
    print("log",log)
    store_test_case(map_name, test_name, r1, r2,log=log)








#Todo: Model all test scenarios. Some have to be hardcoded.
#Make test cases for all the neural nets
#Build pipeline: Gimp -> Tester -> Net -> Output
#os.system("""osascript -e 'tell app "GIMP" to open""")
"""
Tests (Grenzwerte anhand von 3 Testkarten):

- leere Karte
- schwarze karte
- leere Route (beide, links, rechts)
- unendlich große Route
- sinnlose Route (nicht kontinuierlich)
- Route kreuzt hindernisse
- selber startpunkt wie zielpunkt
- route macht kreise um hindernisse
- Routen haben nicht die selben Endpunkte
  - unterschiedliche Startpunkte
  - unterschiedliche Zielpunkte
  - Start und Zielpunkt unterschiedlich
- unbekannte Werte
- komische routen
  - viele kurven, aber trotzdem homotop
  - überkreuzen sich oft
  - routen machen kreise

"""
