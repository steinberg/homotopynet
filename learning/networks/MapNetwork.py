import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

class MapNetwork(nn.Module):
    """A simple model, processing bitmaps with several CNN-Layers"""
    def __init__(self, out_features=0, res=1):
        super(MapNetwork, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=12, kernel_size=3) #kernel_size=3, out_channels = (12,32) größer
        self.conv2 = nn.Conv2d(in_channels=12, out_channels=24, kernel_size=5)
        self.conv3 = nn.Conv2d(in_channels=24, out_channels =32, kernel_size=5)
        self.conv4 = nn.Conv2d(in_channels=32, out_channels =48, kernel_size=2) if res <1 else nn.Conv2d(in_channels=32, out_channels =48, kernel_size=5)

        self.fc1 = nn.Linear(in_features=int(48*2*2*res*res), out_features=1000)
        self.fc2 = nn.Linear(in_features=1000, out_features=500)
        self.out = nn.Linear(in_features=500, out_features=out_features)

        self.res = res
        self.preLu = nn.PReLU(num_parameters=1, init=0.25)


    def forward(self, t):
        t = F.max_pool2d(t, kernel_size=int(1/self.res), stride=int(1/self.res))

        t = self.conv1(t)
        t = self.preLu(t)
        t = F.max_pool2d(t, kernel_size= 3, stride = 2)

        t = self.conv2(t)
        t = self.preLu(t)
        t = F.max_pool2d(t, kernel_size= 2, stride = 2)

        t = self.conv3(t)
        t = self.preLu(t)
        t = F.max_pool2d(t, kernel_size= 2, stride = 2)

        t = self.conv4(t)
        t = self.preLu(t)
        t = t if self.res<1 else F.max_pool2d(t, kernel_size= 2, stride = 2)

        #print("Map", t.shape)


        t = self.fc1(t.reshape(t.shape[0],-1))
        t = self.preLu(t)

        t = self.fc2(t)
        t = self.preLu(t)

        t = self.out(t)
        t = self.preLu(t)
        #print("Map prelu", t)

        #Output shape: [bsize, out_channels, 34, 34]
        return t
