import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from MapNetwork import MapNetwork
from RouteNetwork import *

class MainNetworkIndiv(nn.Module):
    """Central Network Architecture, processing routes seperately"""
    def __init__(self, MAP_NN, ROUTE_NN, route_len, out_features=300, res=1):
        super(MainNetworkIndiv, self).__init__()
        self.MAP_NN = MAP_NN(out_features=out_features, res=res)
        self.ROUTE_NN = ROUTE_NN(length=route_len, out_features=out_features)
        self.fc1 = nn.Linear(in_features = 300, out_features = 120)
        self.fc2 = nn.Linear(in_features = 120, out_features = 60)
        self.fc3 = nn.Linear(in_features = 60, out_features = 10)
        self.out = nn.Linear(in_features = 10, out_features = 2)

        self.temp_state = None
        self.route_len = route_len

    #Verarbeiten der Karte und der ersten Route
    def forward_first(self,mps,rt1):
        self.temp_state = self.MAP_NN(mps)
        self.ROUTE_NN.forward_first(rt1)

    #Verarbeiten der zweiten Route, im Anschluss auf forward_first
    def forward_second(self,rt2):
        #process Map
        mps = self.temp_state

        #process Routes
        rts = self.ROUTE_NN.forward_second(rt2)

        #process Map * Route
        t = mps.reshape(mps.shape[0],-1) * rts.reshape(mps.shape[0],-1)
        #print("Cat", t.shape)

        t = self.fc1(t)
        t = F.relu(t)
        #print("Fc1", t)

        t = self.fc2(t)
        t = F.relu(t)
        #print("Fc2", t)

        t = self.fc3(t)
        t = F.relu(t)
        #print("Fc3", t)

        t = self.out(t)
        t = F.softmax(t, dim=1)
        #print("softmax", t)
        return t


    def forward(self, t):
        mps, rt1, rt2 = t

        self.forward_first(mps,rt1)
        t = self.forward_second(rt2)

        return t

class MainNetworkCNNSingle(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkCNNSingle, route_len=route_len)

class MainNetworkCNNSiamese(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkCNNSiamese, route_len=route_len)

class MainNetworkCNN2Channel(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkCNN2Channel, route_len=route_len)

class MainNetworkCSTSSiamese(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkCSTSSiamese, route_len=route_len)

class MainNetworkRNNSiamese(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkRNNSiamese, route_len=route_len)

class MainNetworkBaseline(MainNetworkIndiv):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkBaseline, route_len=route_len)



class MainNetworkConcat(nn.Module):
    """Central Network Architecture, processing routes as a polygon"""
    def __init__(self, MAP_NN, ROUTE_NN, route_len, out_features=300):
        super(MainNetworkConcat, self).__init__()
        self.MAP_NN = MAP_NN(out_features=out_features)
        self.ROUTE_NN = ROUTE_NN(length=route_len, out_features=out_features)
        self.fc1 = nn.Linear(in_features = 300, out_features = 120)
        self.fc2 = nn.Linear(in_features = 120, out_features = 60)
        self.fc3 = nn.Linear(in_features = 60, out_features = 10)
        self.out = nn.Linear(in_features = 10, out_features = 2)

        self.route_len = route_len

    def forward(self, t):
        mps, rts = t

        #process Map
        #mps = fill_with_maps(mps, train_set.maps, train_set.names)
        mps = self.MAP_NN(mps)

        #process Route
        rts = self.ROUTE_NN(rts)

        #process Map + Route
        t = mps.reshape(mps.shape[0],-1) * rts.reshape(mps.shape[0],-1)
        #print("Cat", t.shape)

        t = self.fc1(t)
        t = F.relu(t)
        #print("Fc1", t)

        t = self.fc2(t)
        t = F.relu(t)
        #print("Fc2", t)

        t = self.fc3(t)
        t = F.relu(t)
        #print("Fc3", t)

        t = self.out(t)
        t = F.softmax(t, dim=1)
        #print("softmax", t)
        return t

class MainNetworkDouble(MainNetworkConcat):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkConcat, route_len=route_len)


class MainNetworkMultiRes_original(nn.Module):
    #Idea: Alternative routes can be classified on lower dimension, if routes are seperated by big obstacles. This makes faster computation possible
    def __init__(self, MAP_NN, ROUTE_NN, route_len, out_features=300):
        super(MainNetworkMultiRes, self).__init__()
        self.MAIN_NN_low = MainNetworkIndiv(MAP_NN, ROUTE_NN, route_len=route_len, out_features=out_features, res=0.5)
        self.MAIN_NN_high = MainNetworkIndiv(MAP_NN, ROUTE_NN, route_len=route_len, out_features=out_features, res=1)

    def forward(self, t):
        #Process input on lower resolution
        a=t
        a = self.MAIN_NN_low(a)
        #Select which were classified homotopic
        ix = [i for i,y in enumerate(a.argmax(1)) if y==1]
        #Process those again on high resolution
        if ix:
            h = [t[0][ix], t[1][ix], t[2][ix]]
            h = self.MAIN_NN_high(h)
            print("Processed b")
            #Merge results
            a[ix] = h

        return a

class MainNetworkMultiRes(nn.Module):
    def __init__(self, MAP_NN, ROUTE_NN, route_len, out_features=300, res=1):
        super(MainNetworkMultiRes, self).__init__()
        self.MAP_NN_high = MAP_NN(out_features=out_features, res=1)
        self.MAP_NN_low = MAP_NN(out_features=out_features, res=0.5)
        self.ROUTE_NN = ROUTE_NN(length=route_len, out_features=out_features)
        self.fc0 = nn.Linear(in_features = 600, out_features = 300)
        self.fc1 = nn.Linear(in_features = 300, out_features = 120)
        self.fc2 = nn.Linear(in_features = 120, out_features = 60)
        self.fc3 = nn.Linear(in_features = 60, out_features = 10)
        self.out = nn.Linear(in_features = 10, out_features = 2)

        self.temp_state = None

    #Verarbeiten der Karte und der ersten Route
    def forward_first(self,mps,rt1):
        self.temp_state = self.MAP_NN_high(mps), self.MAP_NN_low(mps)
        self.ROUTE_NN.forward_first(rt1)

    #Verarbeiten der zweiten Route, im Anschluss auf forward_first
    def forward_second(self,rt2):
        #process Map
        mps_high, mps_low = self.temp_state

        #process Routes
        rts = self.ROUTE_NN.forward_second(rt2)

        #process Map * Route
        t = torch.cat((mps_high.reshape(mps_high.shape[0],-1) * rts.reshape(mps_high.shape[0],-1), mps_low.reshape(mps_high.shape[0],-1) * rts.reshape(mps_high.shape[0],-1)),1)

        t = self.fc0(t)
        t = F.relu(t)

        t = self.fc1(t)
        t = F.relu(t)
        #print("Fc1", t)

        t = self.fc2(t)
        t = F.relu(t)
        #print("Fc2", t)

        t = self.fc3(t)
        t = F.relu(t)
        #print("Fc3", t)

        t = self.out(t)
        t = F.softmax(t, dim=1)
        #print("softmax", t)
        return t

    def forward(self, t):
        mps, rt1, rt2 = t

        self.forward_first(mps,rt1)
        t = self.forward_second(rt2)

        return t

class MainNetworkCNN2Channel_MR(MainNetworkMultiRes):
    def __init__(self, route_len):
        super().__init__(MAP_NN = MapNetwork, ROUTE_NN = RouteNetworkCNN2Channel, route_len=route_len)
