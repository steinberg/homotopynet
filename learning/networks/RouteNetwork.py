import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import preprocessing as prep
class RouteNetworkConcat(nn.Module):
    """Route Network base-architecture, processing concatenated routes"""
    def __init__(self,length, out_features=0):
        #For each conv/pool-layer: output_len = (input_len - kernel_size)/stride +1
        conv_size = int((((((length-3+1)-2)/2+1-3+1-2)/2+1-3+1-2)/2+1-3+1-2)/2+1)

        super(RouteNetworkConcat, self).__init__()
        self.conv1 = nn.Conv1d(in_channels=1, out_channels=6, kernel_size=3)
        self.conv2 = nn.Conv1d(in_channels=6, out_channels=12, kernel_size=3)
        self.conv3 = nn.Conv1d(in_channels=12, out_channels=24, kernel_size=3)
        self.conv4 = nn.Conv1d(in_channels=24, out_channels=32, kernel_size=3)
        self.fc1 = nn.Linear(in_features=32*conv_size, out_features=3000)
        self.fc2 = nn.Linear(in_features=3000, out_features=2000)
        self.out = nn.Linear(in_features=2000, out_features=out_features)

    def forward(self,t):
        t = t

        t = self.conv1(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)


        t = self.conv2(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        t = self.conv3(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        t = self.conv4(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        #print("Route", t.shape)

        t = self.fc1(t.reshape(t.shape[0],-1))
        t = F.relu(t)

        t = self.fc2(t)
        t = F.relu(t)

        t = self.out(t)
        t = F.relu(t)
        #print("Route Prelu", t)

        #Output shape: [bsize, out_channels, 78]

        return t

class RouteNetworkCNN(nn.Module):
    """Route Network base-architecture using CNN"""
    def __init__(self, length, out_features=0, channels=1):
        #For each conv/pool-layer: output_len = (input_len - kernel_size)/stride +1
        conv_size = int((int(( int(( int(((length-3+1)-2)/2+1) -3+1-2)/2+1)-3+1-2)/2+1)-3+1-2)/2+1)
        #print("Conv_size",conv_size)
        cn = channels
        super(RouteNetworkCNN, self).__init__()
        self.conv1 = nn.Conv1d(in_channels=cn, out_channels=6*cn, kernel_size=3)
        self.conv2 = nn.Conv1d(in_channels=6*cn, out_channels=12*cn, kernel_size=3)
        self.conv3 = nn.Conv1d(in_channels=12*cn, out_channels=24*cn, kernel_size=3)
        self.conv4 = nn.Conv1d(in_channels=24*cn, out_channels=32*cn, kernel_size=3)
        self.fc1 = nn.Linear(in_features=32*conv_size*cn, out_features=2000*cn)
        self.fc2 = nn.Linear(in_features=2000*cn, out_features=1000)
        self.out = nn.Linear(in_features=1000, out_features=out_features)

    def forward(self,t):
        t = t

        t = self.conv1(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)
        #print("prelu", t)

        t = self.conv2(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        t = self.conv3(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        t = self.conv4(t)
        t = F.relu(t)
        t = F.max_pool1d(t, kernel_size = 2, stride = 2)

        #print("Route", t.shape)

        t = self.fc1(t.reshape(t.shape[0],-1))
        t = F.relu(t)

        t = self.fc2(t)
        t = F.relu(t)

        t = self.out(t)
        t = F.relu(t)
        #print("Route Prelu", t)

        return t

class RouteNetworkSurround(nn.Module):
    """Part of Central-surround two-stream (Network adapted from Zagoruyko et al.)"""
    def __init__(self, length, out_features=0):
        #For each conv/pool-layer: output_len = (input_len - kernel_size)/stride +1
        conv_size = int((int(( int(( int(((length-3+1)-2)/2+1) -3+1-2)/2+1)-3+1-2)/2+1)-3+1-2)/2+1)
        #print("conv size surround", conv_size)
        super(RouteNetworkSurround, self).__init__()

        self.conv1_s = nn.Conv1d(in_channels=1, out_channels=6, kernel_size=3)
        self.conv2_s = nn.Conv1d(in_channels=6, out_channels=12, kernel_size=3)
        self.conv3_s = nn.Conv1d(in_channels=12, out_channels=24, kernel_size=3)
        self.conv4_s = nn.Conv1d(in_channels=24, out_channels=32, kernel_size=3)

        self.out = nn.Linear(in_features=32*conv_size, out_features=out_features) #1500

    def forward(self,s):

        s = self.conv1_s(s)
        s = F.relu(s)
        s = F.max_pool1d(s, kernel_size = 2, stride = 2)

        s = self.conv2_s(s)
        s = F.relu(s)
        s = F.max_pool1d(s, kernel_size = 2, stride = 2)

        s = self.conv3_s(s)
        s = F.relu(s)
        s = F.max_pool1d(s, kernel_size = 2, stride = 2)

        s = self.conv4_s(s)
        s = F.relu(s)
        s = F.max_pool1d(s, kernel_size = 2, stride = 2)

        #print("Surround", s.shape)

        s = self.out(s.reshape(s.shape[0],-1))
        s = F.relu(s)

        return s

class RouteNetworkCenter(nn.Module):
    """Part of Central-surround two-stream (Network adapted from Zagoruyko et al.)"""
    def __init__(self, length, out_features=0):
        #For each conv/pool-layer: output_len = (input_len - kernel_size)/stride +1
        conv_size = int((int(( int(( int(((length-3+1)-2)/2+1) -3+1-2)/2+1)-3+1-2)/2+1)-3+1-2)/2+1)
        #print("conv_size center", conv_size)
        super(RouteNetworkCenter, self).__init__()

        self.avlen = int(prep._len_avg())
        self.conv1_c = nn.Conv1d(in_channels=1, out_channels=6, kernel_size=3)
        self.conv2_c = nn.Conv1d(in_channels=6, out_channels=12, kernel_size=3)
        self.conv3_c = nn.Conv1d(in_channels=12, out_channels=24, kernel_size=3)
        self.conv4_c = nn.Conv1d(in_channels=24, out_channels=32, kernel_size=3)

        self.out = nn.Linear(in_features=32*conv_size, out_features=out_features) #1000

    def forward(self,c):

        c = self.conv1_c(c)
        c = F.relu(c)
        c = F.max_pool1d(c, kernel_size = 2, stride = 2)

        c = self.conv2_c(c)
        c = F.relu(c)
        c = F.max_pool1d(c, kernel_size = 2, stride = 2)

        c = self.conv3_c(c)
        c = F.relu(c)
        c = F.max_pool1d(c, kernel_size = 2, stride = 2)

        c = self.conv4_c(c)
        c = F.relu(c)
        c = F.max_pool1d(c, kernel_size = 2, stride = 2)

        #print("Center", c.shape)

        c = self.out(c.reshape(c.shape[0],-1))
        c = F.relu(c)

        return c

class RouteNetworkRNN(nn.Module):
    """Route Network base-architecture using CNN"""
    def __init__(self, length, out_features=0):
        super(RouteNetworkRNN, self).__init__()

        hidden_dim = out_features

        self.lstm = nn.LSTM(100*100, 80)

        self.fc1 = nn.Linear(in_features=80, out_features=1000)
        self.fc2 = nn.Linear(in_features=1000, out_features=500)
        self.out = nn.Linear(in_features=500, out_features=out_features)

    def one_hot(self, route):
        embedded = list()
        #each one_hot vector has one entry for each point on the map
        embedded.append(torch.zeros(100*100))
        for point in prep.normalize_route_values(route):
            embed_point = torch.zeros(100*100)
            embed_point[point] = 1
            embedded.append(embed_point)
        return torch.stack(embedded)

    def forward(self,t):
        batch = list()
        for batch_part in t:
            route = batch_part.squeeze()
            embeds = self.one_hot(route)
            _, (batch_part,_) = self.lstm(embeds.view(-1,1,100*100))
            batch.append(batch_part)

        t = torch.stack(batch)
        t = F.relu(t)

        t = self.fc1(t)
        t = F.relu(t)

        t = self.fc2(t)
        t = F.relu(t)

        t = self.out(t)
        t = F.relu(t)

        return t

### Adapted Networks ###
class RouteNetworkIndiv(nn.Module):
    """Processes both Routes individually"""
    def __init__(self, length, out_features=0):
        super(RouteNetworkIndiv,self).__init__()
        self.temp_state = None

    def forward_first(self,rt1):
        pass

    def forward_second(self,rt2):
        pass

    def forward(self,t):
        rt1, rt2 = t
        self.forward_first(rt1)
        t = self.forward_second(rt2)
        return t

class RouteNetworkCNNSingle(RouteNetworkIndiv):
    """... and uses seperated weights."""
    def __init__(self, length, out_features=0):
        super().__init__(length)
        self.ROUT_NN1 = RouteNetworkCNN(length=length, out_features = 200)
        self.ROUT_NN2 = RouteNetworkCNN(length=length, out_features = 200)
        self.fc_routes = nn.Linear(in_features=200, out_features=out_features)

    def forward_first(self,rt1):
        self.temp_state = self.ROUT_NN1(rt1)

    def forward_second(self,rt2):
        rt1 = self.temp_state
        rt2 = self.ROUT_NN2(rt2)
        rts = rt1 * rt2
        rts = self.fc_routes(rts)
        rts = F.relu(rts)
        return rts

class RouteNetworkCNNSiamese(RouteNetworkIndiv):
    """... and uses shared weights."""
    def __init__(self, length, out_features=0):
        super().__init__(length)
        self.ROUT_NN = RouteNetworkCNN(length, out_features = 300)
        self.fc_routes = nn.Linear(in_features=300, out_features=out_features)

    def forward_first(self,rt1):
        self.temp_state = self.ROUT_NN(rt1)

    def forward_second(self,rt2):
        rt1 = self.temp_state
        rt2 = self.ROUT_NN(rt2)
        rts = rt1 * rt2
        rts = self.fc_routes(rts)
        rts = F.relu(rts)
        return rts



class RouteNetworkCSTSSiamese(RouteNetworkIndiv):
    """Central-Surround Two-Stream adaption (Zagoruyko-2015), with shared weights"""
    def __init__(self, length, out_features=0):

        super().__init__(length)
        self.avlen = int(prep._len_avg())
        self.CSTS_center = RouteNetworkCenter(length=self.avlen, out_features = 1000)
        self.CSTS_surround = RouteNetworkSurround(length=length-self.avlen, out_features = 1500)

        self.fc1 = nn.Linear(in_features=2500, out_features=2000)
        self.fc2 = nn.Linear(in_features=2000, out_features=1000)
        self.fc3 = nn.Linear(in_features=1000, out_features=500)
        self.out = nn.Linear(in_features=500, out_features=out_features)

    def get_c_and_s(self, t):
        tlen = t.shape[2]
        mid = int((tlen)/2)
        l = mid-int(self.avlen/2)
        r = mid+(self.avlen-int(self.avlen/2))
        center = t[:,:,l:r]
        surround = torch.cat((t[:,:,:l], t[:,:,r:]), dim=-1)
        return center, surround


    def forward_first(self,rt1):
        cen1, sur1 = self.get_c_and_s(rt1)
        cen1 = self.CSTS_center(cen1)
        sur1 = self.CSTS_surround(sur1)
        self.temp_state = (cen1, sur1)

    def forward_second(self,rt2):
        cen1, sur1 = self.temp_state
        cen2, sur2 = self.get_c_and_s(rt2)

        cen2 = self.CSTS_center(cen2)
        c = cen1 * cen2

        sur2 = self.CSTS_surround(sur2)
        s = sur1 * sur2

        t = torch.cat((s,c), dim=-1)

        t = self.fc1(t)
        t = F.relu(t)

        t = self.fc2(t)
        t = F.relu(t)

        t = self.fc3(t)
        t = F.relu(t)

        t = self.out(t)
        t = F.relu(t)

        return t

class RouteNetworkCNN2Channel(RouteNetworkIndiv):
    """Treats each route as a color channel (Zagoruyko-2015)"""
    def __init__(self, length, out_features=0):
        super().__init__(length)
        self.ROUT_NN = RouteNetworkCNN(length, out_features = 400, channels = 2)
        self.fc_routes = nn.Linear(in_features=400, out_features=out_features)

    def forward_first(self,rt1):
        self.temp_state = rt1

    def forward_second(self,rt2):
        rt1 = self.temp_state
        rtc = torch.cat((rt1,rt2), dim=1)
        rtc = self.ROUT_NN(rtc)
        rtc = self.fc_routes(rtc)
        rtc = F.relu(rtc)
        return rtc


class RouteNetworkRNNSiamese(RouteNetworkIndiv):
    """Route Network using RNN & shared weights"""
    def __init__(self, length, out_features=0):
        super().__init__(length)
        self.ROUT_RNN = RouteNetworkRNN(length, out_features = 200)
        self.fc_routes = nn.Linear(in_features=200, out_features=out_features)

    def forward_first(self,rt1):
        self.temp_state = self.ROUT_RNN(rt1)

    def forward_second(self,rt2):
        rt1 = self.temp_state
        rt2 = self.ROUT_RNN(rt2)
        rts = rt1 * rt2
        rts = self.fc_routes(rts)
        rts = F.relu(rts)
        return rts


class RouteNetworkBaseline(RouteNetworkIndiv):
    def __init__(self, length, out_features=0):
        super().__init__(length)
        self.fc_routes = nn.Linear(in_features=867, out_features=300)

    def forward_first(self,rt1):
        self.temp_state = rt1

    def forward_second(self,rt2):
        rt1 = self.temp_state
        rts = rt1 * rt2
        rts = self.fc_routes(rts)
        rts = torch.ones(rts.shape)
        return rts
