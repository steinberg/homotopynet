import matplotlib.pyplot as plt
import numpy as np

from collections import deque
from routes import *
import random, math
import os, json

from preprocessing import normalize_route
#Locations for storing maps
path_maps = "dataset/maps/"
path_maps_raw = path_maps + "raw/"
path_maps_prep = path_maps + "preprocessed/"
path_maps_aug = path_maps + "preprocessed/augmented/"

#Locations for storing routes
path_routes = "dataset/raw/"
path_routes_raw = path_routes + "raw/"
path_routes_prep = path_routes + "preprocessed/"
path_routes_aug = path_routes + "augmented/"

def plt_route(bitmap, *route, label="", endings=True, fp=None, vertices=False):
    """Plot several routes onto a bitmap"""
    fl = lambda l: np.array(list(l)[:1]+list(l)[-1:])
    bitmap = np.invert(bitmap)

    plt.imshow(bitmap, cmap='gray')
    for i, r in enumerate(route):
        cols, rows = np.array([obs[0] for obs in r]),np.array([obs[1] for obs in r])
        if vertices:
            plt.scatter(rows,cols)
        else:
            plt.plot(rows,cols)
        if endings:
            plt.scatter(fl(rows), fl(cols), color="green")

    plt.title(label)
    plt.axis('off')
    if fp:
        plt.savefig(fp)
    else:
        plt.show()
    plt.close()

def plt_route_list(bitmap, routes, label="", endings=True, fp=None):
    """Plot a list of routes onto a bitmap"""
    fl = lambda l: np.array(list(l)[:1]+list(l)[-1:])
    bitmap = np.invert(bitmap)

    plt.imshow(bitmap, cmap='gray')
    for i, r in enumerate(routes):
        cols, rows = np.array([obs[0] for obs in r]),np.array([obs[1] for obs in r])
        plt.plot(rows,cols)
        if endings:
            plt.scatter(fl(rows), fl(cols), color="green")

    plt.title(label)
    plt.axis('off')
    if fp:
        plt.savefig(fp)
    else:
        plt.show()
    plt.close()

def plt_both_routes(bitmap, route1, route2, label="Some_Map", fp=None):
    """Plot a pair of routes, each in a seperate subgraph"""
    bitmap = np.invert(bitmap)
    fl = lambda l: np.array(list(l)[:1]+list(l)[-1:])
    md = lambda l: np.array(list(l)[1:-1])
    fig, (ax1, ax2) = plt.subplots(2)
    fig.suptitle(label)

    ax1.imshow(bitmap, cmap='gray')
    cols, rows = np.array([pos[0] for pos in route1]), np.array([pos[1] for pos in route1])
    ax1.plot(md(rows), md(cols), color="blue")
    ax1.scatter(fl(rows), fl(cols), color="green")

    ax2.imshow(bitmap, cmap='gray')
    cols, rows = np.array([pos[0] for pos in route2]), np.array([pos[1] for pos in route2])
    ax2.plot(md(rows), md(cols), color="red")
    ax2.scatter(fl(rows), fl(cols), color="green")
    plt.axis('off')
    if fp:
        plt.savefig(fp)
    else:
        plt.show()
    plt.close()


def plt_point(bitmap, *points):
    """Plot several points onto a bitmap"""
    print([point for point in points])
    plt_route(bitmap, [point for point in points])

def plt_maps(fp=None):
    """Plot all raw maps"""
    from routes import complex_room
    import os
    for map in os.listdir(path_maps_raw):
        if map.endswith(".bmp"):
            cor = complex_room()
            cor.from_file(path_maps_raw+map)
            plt.title(map)
            plt.imshow(cor.get_room(), cmap="gray")
            if fp:
                plt.savefig(fp)
            else:
                plt.show()
    plt.close()

def plt_indices(dataset, indices, n_cols=5, label=None, fp=None):
    """Plot a certain batch of route pairs from a dataset, each in a seperate subgraph"""
    fl = lambda l: np.array(list(l)[:1]+list(l)[-1:])
    cols = min(len(indices), n_cols)
    if cols == 0:
        return None
    if dataset == None:
        print("Dataset is None")
        return None
    rows = math.ceil(len(indices)/cols)
    fig, axes = plt.subplots(rows, cols)
    if label:
        fig.suptitle(label)
    for n, i in enumerate(indices):
        r,c = int(n/cols), int(n%cols)
        if rows <= 1:
            ax = axes[c]
        else:
            ax = axes[r,c]
        data, _ = dataset[i]
        bitmap, rt1, rt2 = data
        bitmap = bitmap.squeeze().numpy().astype(int)
        route1 = normalize_route(rt1.squeeze())
        route2 = normalize_route(rt2.squeeze())
        bitmap = np.invert(bitmap)
        ax.axis('off')
        ax.imshow(bitmap, cmap='gray')
        cc, rr = np.array([pos[0] for pos in route1]), np.array([pos[1] for pos in route1])
        ax.plot(rr, cc, color="blue")
        cc, rr = np.array([pos[0] for pos in route2]), np.array([pos[1] for pos in route2])
        ax.plot(rr, cc, color="red")
        ax.scatter(fl(rr), fl(cc), color="green")
    if fp:
        print("saving fig to", fp)
        plt.savefig(fp)
    else:
        plt.show()
    plt.close()



def plt_sample_batch(train_set, test_set, total=50, n_cols=10, fp=None):
    """Plot a random batch of route-pairs representing Train- and Testset"""
    import math
    from torch.utils.data import DataLoader, Dataset
    fl = lambda l: np.array(list(l)[:1]+list(l)[-1:])
    plt.axis('off')
    cols = min(total, n_cols)
    if cols == 0:
        return None
    rows = math.ceil(total/cols)
    fig, axes = plt.subplots(rows, cols)

    n_train = int(total * 0.7)
    train_loader = DataLoader(train_set, batch_size=n_train, shuffle=True)
    test_loader = DataLoader(test_set, batch_size=total-n_train, shuffle=True)

    train_data, _ = next(iter(train_loader))
    map_train, rts1_train, rts2_train = train_data

    for i in range(n_train):
        r,c = int(i/cols), int(i%cols)
        ax = axes[r,c]
        bitmap = map_train[i].squeeze().numpy().astype(int)
        route1 = normalize_route(rts1_train[i].squeeze())
        route2 = normalize_route(rts2_train[i].squeeze())
        #print(bitmap.shape, np.unique(bitmap))
        bitmap = np.invert(bitmap)
        ax.axis('off')
        ax.imshow(bitmap, cmap='gray')
        cc, rr = np.array([pos[0] for pos in route1]), np.array([pos[1] for pos in route1])
        ax.plot(rr, cc, color="blue")
        cc, rr = np.array([pos[0] for pos in route2]), np.array([pos[1] for pos in route2])
        ax.plot(rr, cc, color="red")
        ax.scatter(fl(rr), fl(cc), color="green")

    test_data, _ = next(iter(test_loader))
    map_test, rts1_test, rts2_test = test_data

    for j, i in enumerate(range(n_train, total)):
        r,c = int(i/cols), int(i%cols)
        ax = axes[r,c]
        bitmap = map_test[j].squeeze().numpy().astype(int)
        route1 = normalize_route(rts1_test[j].squeeze())
        route2 = normalize_route(rts2_test[j].squeeze())

        bitmap = np.invert(bitmap)

        ax.axis('off')
        ax.imshow(bitmap, cmap='gray')
        cc, rr = np.array([pos[0] for pos in route1]), np.array([pos[1] for pos in route1])
        ax.plot(rr, cc, color="blue")
        cc, rr = np.array([pos[0] for pos in route2]), np.array([pos[1] for pos in route2])
        ax.plot(rr, cc, color="red")
        ax.scatter(fl(rr), fl(cc), color="green")
    if fp:
        plt.savefig(fp)
    else:
        plt.show()
    plt.close()


def plt_penalty():
    """Plot sample application of penalty algorithm"""
    for _ in range(5):
        map_name = random.choice([path for path in os.listdir(path_maps_raw) if path.endswith(".bmp")])[:-4]
        cor = complex_room.from_file(path_maps_raw+map_name+".bmp")
        bm = cor.get_room()
        bg = bit_graph(cor)
        start, goal = cor.rand_pos(), cor.rand_pos()
        while not bg.exists_path(start, goal):
            start, goal = cor.rand_pos(), cor.rand_pos()
        routes = cor.penalty(bg, start, goal, limit=30)
        print(len(routes))
        plt_route_list(bm, routes)

def plt_one_patching():
    """Plot sample application of one-patching algorithm"""
    for _ in range(5):
        map_name = random.choice([path for path in os.listdir(path_maps_raw) if path.endswith(".bmp")])[:-4]
        cor = complex_room.from_file(path_maps_raw+map_name+".bmp")
        bm = cor.get_room()
        bg = bit_graph(cor)
        start, goal = cor.rand_pos(), cor.rand_pos()
        while not bg.exists_path(start, goal):
            start, goal = cor.rand_pos(), cor.rand_pos()
        routes = cor.penalty(bg, start, goal, limit=30)
        print(len(routes))
        plt_route_list(bm, routes)
